module.exports = {
  root: true,
  ignorePatterns: ['*.min.js'],
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
  },
  extends: ['eslint:recommended', 'plugin:tailwindcss/recommended'],
  rules: {
    'tailwindcss/classnames-order': 'off', // Respect prettier-plugin-tailwindcss order
  },
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 'latest',
  },
  plugins: ['tailwindcss'],
};

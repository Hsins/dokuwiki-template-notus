<?php

namespace dokuwiki\template\notus;

use Twig\Loader\FilesystemLoader;

/**
 * Custom loader that takes the DokuWiki config into account
 */
class NotusLoader extends FilesystemLoader
{
    /**
     * Cache is dependent on DokuWiki config
     * @inheritdoc
     */
    public function isFresh( string $name, int $time ): bool
    {
        $fresh = parent::isFresh( $name, $time );
        if ( !$fresh ) {
            return $fresh;
        }

        $ctime = @filemtime( DOKU_CONF . 'local.php' );

        return ( $time > $ctime );
    }
}

<?php

use dokuwiki\template\notus\TemplateController;

$TemplateController = new TemplateController( basename( __FILE__, '.php' ) );
$TemplateController->render();
